#include <jni.h>
#include <string>
#include <android/log.h>
#include <encode_video_c.h>


extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
}


extern "C" JNIEXPORT jstring JNICALL
Java_co_ffive_videogen_VideoGenerator_generateVideoJNI(
        JNIEnv *env,
        jobject /* this */) {



    /* find the mpeg1video encoder */
    //  codec =  avcodec_find_encoder_by_name(codec_name);

    std::string hello = "done";

    return env->NewStringUTF(hello.c_str());
}


/**
 * This method waits for :
 *todo: output path, time total
 *
 * @param [palette] - int[] 32bits, needs shift to become *!unsigned!* to represent BGRA from Dart
 *
 * @param [argb] - self-coded format, where each pixel represented by bits:
 *          0...7   (     & 0xFF  ) - u_int_8  coded index of color in palette (0-255 max.)
 *          8...23  (<<16 & 0xFFFF) - u_int_16 coded coordinates (0...255*255 max.)
 *          24...   (<<24 & 0xFF  ) - u_int_8 coded bool placement state (0 - misplaced, !=0 placed)
 *
 * @param [history] - string path to file with history (32 bits for 1 edit), same format as  above
 *
 * @param [width,height] - da izi je
 *
 */
extern "C" JNIEXPORT jdouble JNICALL
Java_co_ffive_videogen_VideoGenerator_historyToVideoJNI(JNIEnv *env, jclass type,
                                                        jintArray argb, jstring jpath_,
                                                        jintArray palette_, jint width,
                                                        jint height) {
    const char *jpath = env->GetStringUTFChars(jpath_, 0);

    jint *palette = env->GetIntArrayElements(palette_, NULL);
    jint *rgb = env->GetIntArrayElements(argb, NULL);

    // TODO


    VideoCreator::run_encoding(jpath, (uint32_t *) rgb, (uint32_t *) palette, width, height);
    env->ReleaseStringUTFChars(jpath_, jpath);
    env->ReleaseIntArrayElements(palette_, palette, 0);
    return 0.0;
}
