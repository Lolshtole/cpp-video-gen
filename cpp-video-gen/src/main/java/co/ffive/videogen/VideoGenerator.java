package co.ffive.videogen;

public class VideoGenerator {


    static {
        System.loadLibrary("native-lib");
    }

    public static void generate(VideoParams params) {

        double result = historyToVideoJNI(
                params.original,
                params.history,
                params.palette,
                params.originalWidth,
                params.originalHeight
        );

    }


    public native String generateVideoJNI();

    public static native double historyToVideoJNI(int[] argb, String jpath, int[] palette, int width, int originalHeight);


}
