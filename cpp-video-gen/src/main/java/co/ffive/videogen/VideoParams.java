package co.ffive.videogen;

public class VideoParams {

    public String history;
    public int[] original;
    public int[] palette;
    public int originalWidth;
    public int originalHeight;

}
